use rezodb ;

drop table if exists item_category;
create table item_category(
category_id int primary key not null auto_increment,
category_name varchar(256) not null
);

insert into item_category(category_name) values
  ('家具'),
  ('食品'),
  ('本');

 select * from item_category;
