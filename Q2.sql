grant all on rezodb.* to rezodb_user@localhost identified by 'mysqlquestions';
use rezodb ;

drop table if exists item;
create table item(
  item_id int primary key not null auto_increment,
  item_name varchar(256) not null,
  item_price int not null,
  category_id int
);
 select * from item;
