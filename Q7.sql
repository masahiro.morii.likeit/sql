use rezodb ;

drop table if exists item;
create table item(
  item_id int primary key not null auto_increment,
  item_name varchar(256) not null,
  item_price int not null,
  category_id int
);

insert into item(item_name,item_price,category_id) values
  ('堅牢な机',3000,1),
  ('生焼け肉',50,2),
  ('すっきりわかるJava入門',3000,3),
  ('おしゃれな椅子',2000,1),
  ('こんがり肉',500,2),
  ('書き方ドリルSQL',2500,3),
  ('ふわふわのベッド',30000,1),
  ('ミラノ風ドリア',300,2);

 select item_name, item_price from item where item_name like('%肉%');
