grant all on rezodb.* to rezodb_user@localhost identified by 'mysqlquestions';
use rezodb ;

drop table if exists item_category;
create table item_category(
category_id int primary key not null auto_increment,
category_name varchar(256) not null
);

select * from item_category;
