use rezodb ;


drop table if exists item_category;
create table item_category(
category_id int primary key not null auto_increment,
category_name varchar(256) not null
);

insert into item_category(category_name) values
  ('家具'),
  ('食品'),
  ('本');

  drop table if exists item;
  create table item(
    item_id int primary key not null auto_increment,
    item_name varchar(256) not null,
    item_price int not null,
    category_id int
  );

insert into item(item_name,item_price,category_id) values
  ('堅牢な机',3000,1),
  ('生焼け肉',50,2),
  ('すっきりわかるJava入門',3000,3),
  ('おしゃれな椅子',2000,1),
  ('こんがり肉',500,2),
  ('書き方ドリルSQL',2500,3),
  ('ふわふわのベッド',30000,1),
  ('ミラノ風ドリア',300,2);

 select item_id, item_name, item_price, category_name from item join item_category on item.category_id = item_category.category_id;
